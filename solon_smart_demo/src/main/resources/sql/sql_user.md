get_user
===
```sql
SELECT user_id,role_id,m.nickname,n.role_name,n.is_super FROM ssd_user_role s
LEFT JOIN ssd_user m ON s.user_id = m.id
LEFT JOIN ssd_role n ON s.role_id = n.id
WHERE s.user_id = #{user_id} limit 1
```

get_menus
===
```sql
SELECT s.role_id,s.menu_id as id,top_id,top_lv,is_top,menu_name as name,link,rev FROM ssd_role_menu s
LEFT JOIN ssd_menu m ON s.menu_id = m.id
WHERE s.role_id = #{role_id} ORDER BY id
```

