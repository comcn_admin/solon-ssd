CREATE TABLE `appx` (
                        `app_id` INT NOT NULL AUTO_INCREMENT COMMENT '应用ID',
                        `app_key` VARCHAR(40) DEFAULT NULL COMMENT '应用访问KEY',
                        `akey` VARCHAR(40) DEFAULT NULL COMMENT '（用于取代app id 形成的唯一key） //一般用于推广注册之类',
                        `ugroup_id` INT DEFAULT '0' COMMENT '加入的用户组ID',
                        `agroup_id` INT DEFAULT NULL COMMENT '加入的应用组ID',
                        `name` VARCHAR(50) DEFAULT NULL COMMENT '应用名称',
                        `note` VARCHAR(50) DEFAULT NULL COMMENT '应用备注',
                        `ar_is_setting` INT NOT NULL DEFAULT '0' COMMENT '是否开放设置',
                        `ar_is_examine` INT NOT NULL DEFAULT '0' COMMENT '是否审核中(0: 没审核 ；1：审核中)',
                        `ar_examine_ver` INT NOT NULL DEFAULT '0' COMMENT '审核 中的版本号',
                        `log_fulltime` DATETIME DEFAULT NULL,
                        PRIMARY KEY (`app_id`),
                        UNIQUE KEY `IX_akey` (`akey`) USING BTREE
) DEFAULT CHARSET=utf8  COMMENT='应用表';


CREATE TABLE `ssd_user` (
                            `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户ID',
                            `passport` VARCHAR(45) NOT NULL COMMENT '用户账号',
                            `pwd` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户密码',
                            `nickname` VARCHAR(45) NOT NULL COMMENT '用户昵称',
                            `create_at` DATETIME DEFAULT NULL COMMENT '创建时间',
                            `update_at` DATETIME DEFAULT NULL COMMENT '更新时间',
                            PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COMMENT '用户表';


CREATE TABLE `ssd_role` (
                            `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '角色Id',
                            `role_name` VARCHAR(45) NOT NULL COMMENT '角色名称',
                            `is_super` SMALLINT DEFAULT 0 NOT NULL COMMENT '是否超管 1:是 默认：0 否',
                            `create_at` DATETIME DEFAULT NULL COMMENT '创建时间',
                            `update_at` DATETIME DEFAULT NULL COMMENT '更新时间',
                            PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COMMENT '角色表';


CREATE TABLE `ssd_menu` (
                            `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '菜单Id',
                            `top_id` INT UNSIGNED NOT NULL  COMMENT '上级菜单Id',
                            `top_lv` INT UNSIGNED NOT NULL DEFAULT 1  COMMENT '2级菜单 0 最上级，1子级',
                            `menu_name` VARCHAR(45) NOT NULL COMMENT '菜单名称',
                            `is_top` SMALLINT DEFAULT 0 NOT NULL COMMENT '顶部菜单 1:是 默认：0 否',
                            `link` VARCHAR(100)  NOT NULL COMMENT '连接地址URL',
                            `rev` INT  NOT NULL DEFAULT 800 COMMENT '属性rev设置iframe高度',
                            `create_at` DATETIME DEFAULT NULL COMMENT '创建时间',
                            `update_at` DATETIME DEFAULT NULL COMMENT '更新时间',
                            PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COMMENT '菜单表';


CREATE TABLE `ssd_user_role` (
                                 `user_id` INT NOT NULL,
                                 `role_id` INT NOT NULL,
                                 PRIMARY KEY (`user_id`,`role_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `ssd_role_menu` (
                                 `role_id` INT NOT NULL,
                                 `menu_id` INT NOT NULL,
                                 PRIMARY KEY (`role_id`,`menu_id`)
) DEFAULT CHARSET=utf8 COMMENT '角色菜单';


INSERT INTO `ssd_user`(passport,pwd,nickname) VALUES ('admin','123456','超级管理员');
INSERT INTO `ssd_user`(passport,pwd,nickname) VALUES ('user','123456','普通用户');
INSERT INTO `ssd_role`(role_name,is_super) VALUES ('超级管理员',1);
INSERT INTO `ssd_role`(role_name,is_super) VALUES ('普通用户',0);

INSERT INTO `ssd_user_role`(user_id,role_id) VALUES (1,1);
INSERT INTO `ssd_user_role`(user_id,role_id) VALUES (2,2);

--  truncate table ssd_menu
--  一级菜单
INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (0,0,0,'系统管理','#');
INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (0,0,0,'解决方案','#');
INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (0,0,0,'产品','/product');
INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (0,0,0,'大数据','/bigdata');
INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (0,0,1,'控制台','/console');
INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (0,0,1,'商品管理','/goods');
INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (0,0,1,'用户','/users');
INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (0,0,1,'其他','#');
--  二级菜单
INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (1,1,0,'hello world','/hello');
INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (1,1,0,'hello kugou','/kugou');

INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (2,1,0,'移动模块','/mobile');
INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (2,1,0,'后台模板','/backtemp');
INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (2,1,0,'电商平台','/eplatform');

INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (8,1,1,'邮件管理','/mail');
INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (8,1,1,'消息管理','/message');
INSERT INTO `ssd_menu`(top_id,top_lv,is_top,menu_name,link) VALUES (8,1,1,'授权管理','/grantAuth');

-- truncate table ssd_role_menu
--  角色菜单
INSERT INTO `ssd_role_menu`(role_id,menu_id) SELECT 1,id FROM ssd_menu;

--  第二个角色
INSERT INTO `ssd_role_menu`(role_id,menu_id) SELECT 2,id FROM ssd_menu WHERE id IN (1,9,10);