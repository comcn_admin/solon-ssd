package ssd.dso.dao;

import org.beetl.sql.mapper.BaseMapper;
import ssd.model.AppxModel;

/**
 * @author noear 2021/1/29 created
 */
public interface AppxDao extends BaseMapper<AppxModel> {
}
