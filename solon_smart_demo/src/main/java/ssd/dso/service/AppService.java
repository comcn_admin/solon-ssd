package ssd.dso.service;

import org.beetl.sql.solon.annotation.Db;
import org.noear.solon.aspect.annotation.Service;
import org.noear.solon.data.annotation.Cache;
import org.noear.solon.data.annotation.Tran;
import org.noear.solon.data.tran.TranPolicy;
import ssd.dso.mapper.SqlMapper;

@Service
public class AppService {
    @Db
    SqlMapper mapper;

    public Object getApp(int app_id) throws Exception {
        return mapper.appx_get2(app_id);
    }

    public Object getApp(String app_id) throws Exception {
        return mapper.appx_get2(Integer.parseInt(app_id));
    }

    public void addApp(){
        mapper.appx_add(1);
    }

    @Tran
    public void addApp2(){
        mapper.appx_add(1);
    }

    @Tran(policy = TranPolicy.nested)
    public void addApp3(){
        mapper.appx_add(1);
    }

    @Tran(policy = TranPolicy.requires_new)
    public boolean addApp4(){
        mapper.appx_add(1);
        return true;
    }

    @Cache(seconds = 10)
    @Tran(policy = TranPolicy.requires_new)
    public boolean addApp52(){
        mapper.appx_add(1);
        return true;
    }

    @Tran(policy = TranPolicy.never)
    public void addApp5(){
        mapper.appx_add(1);
    }

    @Tran(policy = TranPolicy.mandatory)
    public void addApp6(){
        mapper.appx_add(1);
    }

    public Object getList()  throws Exception
    {
        return mapper.appx_getlist(1);
    }
}
