package ssd.dso.service;

import org.beetl.sql.solon.annotation.Db;
import org.noear.solon.aspect.annotation.Service;
import org.noear.solon.data.annotation.Cache;
import org.noear.solon.data.annotation.Tran;
import org.noear.solon.data.tran.TranPolicy;
import ssd.dso.mapper.SqlMapper;
import ssd.dso.mapper.UserMapper;
import ssd.model.Menu;
import ssd.model.UserModel;

import java.util.List;

@Service
public class UserService
{
    @Db
    UserMapper mapper;

    public UserModel get_user(int user_id)  throws Exception
    {
        return mapper.get_user(user_id);
    }

    public List<Menu> get_menus(int role_id)  throws Exception
    {
        return mapper.get_menus(role_id);
    }
}
