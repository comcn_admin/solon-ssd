package ssd.dso.mapper;

import org.beetl.sql.mapper.annotation.SqlResource;
import org.beetl.sql.mapper.annotation.Update;
import ssd.model.AppxModel;
import ssd.model.Menu;
import ssd.model.UserModel;

import java.util.List;

@SqlResource("sql_user")
public interface UserMapper
{
    UserModel get_user(int user_id) throws Exception;

    List<Menu> get_menus(int role_id) throws Exception;
}
