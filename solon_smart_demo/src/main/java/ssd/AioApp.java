package ssd;

import org.noear.solon.Solon;
import org.noear.solon.extend.quartz.EnableQuartz;

@EnableQuartz
public class AioApp {
    public static void main(String[] args) {
        Solon.start(AioApp.class, args);
    }
}
