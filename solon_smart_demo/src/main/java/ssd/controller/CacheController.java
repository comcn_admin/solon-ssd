//        Copyright (c) [2022] [wjt]
//        [solon-smart-demo] is licensed under Mulan PubL v2.
//        You can use this software according to the terms and conditions of the Mulan PubL v2.
//        You may obtain a copy of Mulan PubL v2 at:
//        http://license.coscl.org.cn/MulanPubL-2.0
//        THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
//        EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
//        MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
//        See the Mulan PubL v2 for more details.

package ssd.controller;

import lombok.extern.slf4j.Slf4j;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Produces;
import org.noear.solon.core.handle.ModelAndView;
import org.noear.solon.data.annotation.Cache;
import org.noear.solon.data.cache.CacheService;
import ssd.dso.service.AppService;
import ssd.dso.service.UserService;
import ssd.model.Menu;
import ssd.model.UserModel;

import java.util.List;

@Controller
@Slf4j
public class CacheController {

    @Inject
    AppService service;

    @Inject
    CacheService redis;

    //使用默认缓存服务。如果有缓存，直接返回缓存；如果没有，执行函数，缓存结果，并返回
    @Mapping("/redis1")
    @Cache(seconds = 1000)
    public String redis1(String name) throws Exception{
        return "Hello " + name;
    }

    //提醒：如果是实体，实体类要加 toString() 函数！！！
    // 该访问异常 java.io.NotSerializableException: ssd.model.AppxModel
    @Cache(seconds = 1000)
    @Mapping("/redis2")
    public Object redis2() throws Exception{
        return service.getList();
    }

    @Mapping("/redis3")
    public String redis3(String key,String name) throws Exception{
        Object val = redis.get(key);
        if( val!= null) {
            log.warn("缓存生效" + val);
            return val.toString();
        }

        String value = "Hello " + name;
        redis.store(key, value,50000);
        return value;
    }

}
