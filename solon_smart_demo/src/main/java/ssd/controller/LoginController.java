package ssd.controller;

import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.ModelAndView;

@Controller
public class LoginController
{
    @Mapping("/login")
    public Object login() throws Exception
    {
        return new ModelAndView("login.shtm");
    }

    @Mapping("/login/signin")
    public void signin(Context ctx, String name,String pwd) throws Exception
    {
        System.out.println(name);
        System.out.println(pwd);
        ctx.sessionSet("user_name", name);
        ctx.redirect("/");
    }

    @Mapping("/login/signout")
    public void signout(Context ctx) throws Exception
    {
        ctx.sessionClear();
        ctx.redirect("/login");
    }

}
