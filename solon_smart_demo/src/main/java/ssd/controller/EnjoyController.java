package ssd.controller;

import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.ModelAndView;
import ssd.model.UserModel;

@Controller
public class EnjoyController
{
    @Mapping("/enjoy/tab1")
    public Object tab1(){

        ModelAndView vm = new ModelAndView("/layout/tab1.shtm");

        vm.put("name","demo");
        vm.put("message","hello world!");

        return vm;
    }

    @Mapping("/enjoy/tab2")
    public Object tab2(){

        ModelAndView vm = new ModelAndView("layout/tab2.shtm");

        vm.put("name","demo");
        vm.put("message","hello world!");

        return vm;
    }
}
