package ssd.controller;


import lombok.extern.slf4j.Slf4j;
import org.noear.solon.core.handle.Handler;
import org.noear.solon.Utils;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;

@Mapping(value = "**",before = true)
@Slf4j
@Component
public class JwtHandler implements Handler
{
    @Override
    public void handle(Context ctx) throws Exception {
        //如果是登录页则不处理
        if(ctx.path().startsWith("/login")){
            return;
        }


        String user_name = ctx.session("user_name","");
        log.info(user_name);
        if(Utils.isEmpty(user_name)){
            //说明未登录，则终止处理
            //ctx.setHandled(true);

            // 跳转登录页面
            ctx.redirect("/login");
        }
    }

}
