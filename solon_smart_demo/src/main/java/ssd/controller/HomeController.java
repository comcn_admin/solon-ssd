//        Copyright (c) [2022] [wjt]
//        [solon-smart-demo] is licensed under Mulan PubL v2.
//        You can use this software according to the terms and conditions of the Mulan PubL v2.
//        You may obtain a copy of Mulan PubL v2 at:
//        http://license.coscl.org.cn/MulanPubL-2.0
//        THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
//        EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
//        MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
//        See the Mulan PubL v2 for more details.

package ssd.controller;

import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Produces;
import org.noear.solon.core.handle.ModelAndView;
import org.noear.solon.data.annotation.Cache;
import ssd.dso.service.AppService;
import ssd.dso.service.UserService;
import ssd.model.Menu;
import ssd.model.UserModel;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {

    @Inject
    AppService service;

    @Inject
    UserService userService;

    @Produces("text/html;charset=utf-8")
    @Mapping("/")
    public Object home() throws Exception
    {
        int user_id = 1; // 1 or 2
        UserModel m = userService.get_user(user_id);
        List<Menu> menus = userService.get_menus(m.getRole_id());
        List<Menu> top_menus = menus.stream().filter(menu -> menu.getIs_top() == 1).toList();
        List<Menu> side_menus = menus.stream().filter(menu -> menu.getIs_top() == 0).toList();

        ModelAndView vm = new ModelAndView("index.shtm");

//        vm.put("title","demo");
//        vm.put("message","hello world!");
        vm.put("m",m);
        vm.put("top_menus",top_menus);
        vm.put("side_menus",side_menus);

        return vm;
    }

    @Produces("text/html;charset=utf-8")
    @Mapping("/tabs")
    public Object home_tabs() throws Exception
    {
        int user_id = 1;
        UserModel m = userService.get_user(user_id);
        List<Menu> menus = userService.get_menus(m.getRole_id());
        List<Menu> top_menus = menus.stream().filter(menu -> menu.getIs_top() == 1).toList();
        List<Menu> side_menus = menus.stream().filter(menu -> menu.getIs_top() == 0).toList();

        ModelAndView vm = new ModelAndView("index_tabs.shtm");
        vm.put("m",m);
        vm.put("top_menus",top_menus);
        vm.put("side_menus",side_menus);

        return vm;
    }



    @Mapping("/test")
    public Object test() throws Exception{
        return service.getApp(1);
    }

    @Mapping("/test2")
    public Object test2() throws Exception{
        return service.getList();
    }

    //使用默认缓存服务。如果有缓存，直接返回缓存；如果没有，执行函数，缓存结果，并返回
    @Mapping("/redis1")
    @Cache(seconds = 10)
    public String redis1(String name) throws Exception{
        return "Hello " + name;
    }

    //提醒：如果是实体，实体类要加 toString() 函数！！！
    @Cache(seconds = 10)
    @Mapping("/redis2")
    public Object redis2() throws Exception{
        return service.getList();
    }

}
