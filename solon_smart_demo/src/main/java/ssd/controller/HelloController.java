package ssd.controller;

import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.ModelAndView;
import ssd.model.UserModel;

@Controller
public class HelloController
{
    @Mapping("/hello")
    public Object hello(){
        UserModel m = new UserModel();
        m.setId(10);
        m.setName("刘之西东");

        ModelAndView vm = new ModelAndView("hello.shtm");

        vm.put("title","demo");
        vm.put("message","hello world!");
        vm.put("m",m);

        return vm;
    }

    @Mapping("/kugou")
    public Object kugou(){
        UserModel m = new UserModel();


        ModelAndView vm = new ModelAndView("kugou.shtm");

        vm.put("title","demo");
        vm.put("message","hello kugou!");
        vm.put("m",m);

        return vm;
    }

    @Mapping("/product")
    public Object product(){
        UserModel m = new UserModel();


        ModelAndView vm = new ModelAndView("product.shtm");

        vm.put("title","demo");
        vm.put("message","hello product!");
        vm.put("m",m);

        return vm;
    }
}
