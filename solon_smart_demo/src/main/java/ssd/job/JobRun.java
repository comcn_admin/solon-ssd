package ssd.job;

import lombok.extern.slf4j.Slf4j;
import org.noear.solon.extend.quartz.Quartz;

// 简配： s，m，h，d : 秒，分，时，天
@Slf4j
@Quartz(cron7x = "2s" ,name = "job1")
public class JobRun implements Runnable {

    @Override
    public void run() {
        System.out.println("我是 QuartzRun1 （2s）");
    }
}
