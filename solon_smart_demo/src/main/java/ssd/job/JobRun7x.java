package ssd.job;

import lombok.extern.slf4j.Slf4j;
import org.noear.solon.extend.quartz.Quartz;

// cron 支持7位（秒，分，时，日期ofM，月，星期ofW，年）
@Slf4j
@Quartz(cron7x = "0 0/1 * * * ? *")
public class JobRun7x implements Runnable {

    @Override
    public void run() {
        System.out.println("我是 QuartzRun2 （0 0/1 * * * ? *）");
    }
}
