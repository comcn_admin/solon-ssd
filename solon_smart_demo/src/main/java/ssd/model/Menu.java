package ssd.model;

import lombok.Data;

@Data
public class Menu
{
    private String name;

    private String link;

    private int rev;

    private int is_top;

    private int top_id;

    private int top_lv;

    private int id;

}
