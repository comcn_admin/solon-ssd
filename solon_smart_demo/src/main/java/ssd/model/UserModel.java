package ssd.model;

import lombok.Data;

@Data
public class UserModel {
    private long id;
    private int user_id;
    private int role_id;
    private String nickname;
    private String name;
    private String role_name;
    private int is_super;
}
