### 开发日记
    1. 登录页面不是我想要的效果,有时间需要自己研究下css 写一个自己中意的登录页 [todo]
    2. 控制在10kb以内 样式统一 手机端可用 [todo]
    3. jwt 中ctx.session("user_name","") 需要引入 solon.sessionstate.jwt 插件才生效 [ok]
    4. 只要cookie有效期内,重启服务器和重启浏览器 都不影响登录状态 除非退出登录 清楚session state [mk]
    5. 登录密码密文传递 [todo] 
    6. @Cache(seconds = 10) solon.cache.redisson 必须支持超时时间 不然会异常 [mk]
    7. 直接注入 @Inject    CacheService redis 进行 redis 存储和删除操作 源码只有 4 个方法 极简 :) [mk]
    8. 